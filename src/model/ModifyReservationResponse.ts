/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface ModifyReservationResponse {
    Reservation?: models.ReservationsReservation;

    TransactionId?: string;

    TransactionStatus?: ModifyReservationResponse.TransactionStatusEnum;

    RequestGuid?: string;

    Errors?: Array<models.Error>;

    Warnings?: Array<models.Warning>;

    Status?: ModifyReservationResponse.StatusEnum;

}
export namespace ModifyReservationResponse {
    export enum TransactionStatusEnum {
        NUMBER_0 = <any> 0,
        NUMBER_1 = <any> 1,
        NUMBER_2 = <any> 2,
        NUMBER_3 = <any> 3,
        NUMBER_4 = <any> 4,
        NUMBER_5 = <any> 5,
        NUMBER_6 = <any> 6,
        NUMBER_7 = <any> 7,
        NUMBER_8 = <any> 8
    }
    export enum StatusEnum {
        NUMBER_0 = <any> 0,
        NUMBER_1 = <any> 1,
        NUMBER_2 = <any> 2
    }
}
