/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { OTAAddressType } from './oBBLModelsOTAAddressType';
import { OTAAward } from './oBBLModelsOTAAward';
import { OTAHotelAmenityType } from './oBBLModelsOTAHotelAmenityType';
import { OTAHotelAvailRQTPAExtensions } from './oBBLModelsOTAHotelAvailRQTPAExtensions';
import { OTAHotelRef } from './oBBLModelsOTAHotelRef';
import { OTALocation } from './oBBLModelsOTALocation';
import { OTAPositionType } from './oBBLModelsOTAPositionType';
import { OTAProfilesType } from './oBBLModelsOTAProfilesType';
import { OTARadiusType } from './oBBLModelsOTARadiusType';
import { OTARatePlanCandidatesType } from './oBBLModelsOTARatePlanCandidatesType';
import { OTARateRange } from './oBBLModelsOTARateRange';
import { OTARoomAmenityType } from './oBBLModelsOTARoomAmenityType';
import { OTARoomStayCandidatesType } from './oBBLModelsOTARoomStayCandidatesType';
import { OTAStayDateRange } from './oBBLModelsOTAStayDateRange';


export interface OTAHotelSearchCriterion {
    Position?: OTAPositionType;
    Address?: OTAAddressType;
    HotelRefs?: Array<OTAHotelRef>;
    Radius?: OTARadiusType;
    HotelAmenity?: Array<OTAHotelAmenityType>;
    RoomAmenity?: Array<OTARoomAmenityType>;
    Award?: OTAAward;
    ProfilesType?: OTAProfilesType;
    RateRanges?: Array<OTARateRange>;
    StayDateRange?: OTAStayDateRange;
    RoomStayCandidatesType?: OTARoomStayCandidatesType;
    RatePlanCandidatesType?: OTARatePlanCandidatesType;
    Location?: OTALocation;
    GetPricesPerGuest?: boolean;
    TPA_Extensions?: OTAHotelAvailRQTPAExtensions;
}
