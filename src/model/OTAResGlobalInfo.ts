/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface OTAResGlobalInfo {
    TimeSpan?: models.OTATimeSpan;

    Total?: models.OTATotal;

    HotelReservationsIDsType?: models.OTAHotelReservationsIDsType;

    Guarantee?: models.OTAGuarantee;

    CommentsType?: models.OTACommentsType;

}
