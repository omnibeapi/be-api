/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { OTAAmountPercentType } from './oBBLModelsOTAAmountPercentType';
import { OTADeadLine } from './oBBLModelsOTADeadLine';
import { OTAGuaranteeDescription } from './oBBLModelsOTAGuaranteeDescription';
import { OTAGuaranteesAcceptedType } from './oBBLModelsOTAGuaranteesAcceptedType';


export interface OTAGuarantee {
    Start?: Date;
    Duration?: string;
    End?: Date;
    GuaranteeDescription?: OTAGuaranteeDescription;
    GuaranteesAcceptedType?: OTAGuaranteesAcceptedType;
    DeadLine?: OTADeadLine;
    AmountPercent?: OTAAmountPercentType;
    GuaranteeCode?: number;
}
