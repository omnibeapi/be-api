/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface OTAHotelRatePlan {
    SellableProducts?: models.OTASellableProducts;

    ServiceRPHs?: Array<models.OTAServiceRPH>;

    EffectiveDate?: Date;

    ExpireDate?: Date;

    RatePlanName?: string;

    Guarantees?: Array<models.OTAGuarantee>;

    CancelPenalties?: Array<models.OTACancelPenalty>;

    RatePlanInclusions?: Array<models.OTARatePlanInclusion>;

    MealsIncluded?: models.OTAMealsIncluded;

    RatePlanID?: number;

    TaxPolicies?: Array<models.OTATaxPolicy>;

    RatePlanDescription?: models.OTARatePlanDescription;

    AdditionalDetailsType?: models.OTAAdditionalDetailsType;

    Offers?: Array<models.OTAOffer>;

    PaymentPolicies?: models.OTAGuaranteePayment;

    Commission?: models.OTACommission;

    CurrencyCode?: OTAHotelRatePlan.CurrencyCodeEnum;

    RatePlanTypeCode?: number;

    SortOrder?: number;

}
export namespace OTAHotelRatePlan {
    export enum CurrencyCodeEnum {
        NUMBER_1 = <any> 1,
        NUMBER_2 = <any> 2,
        NUMBER_3 = <any> 3,
        NUMBER_4 = <any> 4,
        NUMBER_5 = <any> 5,
        NUMBER_6 = <any> 6,
        NUMBER_7 = <any> 7,
        NUMBER_8 = <any> 8,
        NUMBER_9 = <any> 9,
        NUMBER_10 = <any> 10,
        NUMBER_11 = <any> 11,
        NUMBER_12 = <any> 12,
        NUMBER_13 = <any> 13,
        NUMBER_14 = <any> 14,
        NUMBER_15 = <any> 15,
        NUMBER_16 = <any> 16,
        NUMBER_17 = <any> 17,
        NUMBER_18 = <any> 18,
        NUMBER_19 = <any> 19,
        NUMBER_20 = <any> 20,
        NUMBER_21 = <any> 21,
        NUMBER_22 = <any> 22,
        NUMBER_23 = <any> 23,
        NUMBER_24 = <any> 24,
        NUMBER_25 = <any> 25,
        NUMBER_26 = <any> 26,
        NUMBER_27 = <any> 27,
        NUMBER_28 = <any> 28,
        NUMBER_29 = <any> 29,
        NUMBER_30 = <any> 30,
        NUMBER_31 = <any> 31,
        NUMBER_32 = <any> 32,
        NUMBER_33 = <any> 33,
        NUMBER_34 = <any> 34,
        NUMBER_43 = <any> 43,
        NUMBER_44 = <any> 44,
        NUMBER_46 = <any> 46,
        NUMBER_47 = <any> 47,
        NUMBER_50 = <any> 50,
        NUMBER_52 = <any> 52,
        NUMBER_56 = <any> 56,
        NUMBER_59 = <any> 59,
        NUMBER_62 = <any> 62,
        NUMBER_64 = <any> 64,
        NUMBER_66 = <any> 66,
        NUMBER_72 = <any> 72,
        NUMBER_76 = <any> 76,
        NUMBER_82 = <any> 82,
        NUMBER_83 = <any> 83,
        NUMBER_85 = <any> 85,
        NUMBER_86 = <any> 86,
        NUMBER_91 = <any> 91,
        NUMBER_94 = <any> 94,
        NUMBER_97 = <any> 97,
        NUMBER_98 = <any> 98,
        NUMBER_102 = <any> 102,
        NUMBER_104 = <any> 104,
        NUMBER_108 = <any> 108,
        NUMBER_109 = <any> 109,
        NUMBER_117 = <any> 117,
        NUMBER_118 = <any> 118,
        NUMBER_119 = <any> 119,
        NUMBER_120 = <any> 120,
        NUMBER_121 = <any> 121,
        NUMBER_122 = <any> 122,
        NUMBER_123 = <any> 123,
        NUMBER_124 = <any> 124,
        NUMBER_125 = <any> 125,
        NUMBER_126 = <any> 126,
        NUMBER_127 = <any> 127,
        NUMBER_128 = <any> 128,
        NUMBER_129 = <any> 129,
        NUMBER_130 = <any> 130,
        NUMBER_131 = <any> 131,
        NUMBER_132 = <any> 132,
        NUMBER_133 = <any> 133,
        NUMBER_134 = <any> 134,
        NUMBER_135 = <any> 135,
        NUMBER_136 = <any> 136,
        NUMBER_137 = <any> 137,
        NUMBER_138 = <any> 138,
        NUMBER_139 = <any> 139,
        NUMBER_140 = <any> 140,
        NUMBER_141 = <any> 141,
        NUMBER_142 = <any> 142,
        NUMBER_143 = <any> 143,
        NUMBER_144 = <any> 144,
        NUMBER_145 = <any> 145,
        NUMBER_146 = <any> 146,
        NUMBER_147 = <any> 147,
        NUMBER_148 = <any> 148,
        NUMBER_149 = <any> 149,
        NUMBER_150 = <any> 150,
        NUMBER_151 = <any> 151,
        NUMBER_152 = <any> 152,
        NUMBER_153 = <any> 153,
        NUMBER_154 = <any> 154,
        NUMBER_155 = <any> 155,
        NUMBER_156 = <any> 156,
        NUMBER_157 = <any> 157,
        NUMBER_158 = <any> 158,
        NUMBER_159 = <any> 159,
        NUMBER_160 = <any> 160,
        NUMBER_161 = <any> 161,
        NUMBER_162 = <any> 162,
        NUMBER_163 = <any> 163,
        NUMBER_164 = <any> 164,
        NUMBER_165 = <any> 165,
        NUMBER_166 = <any> 166,
        NUMBER_167 = <any> 167,
        NUMBER_168 = <any> 168,
        NUMBER_169 = <any> 169,
        NUMBER_170 = <any> 170,
        NUMBER_171 = <any> 171,
        NUMBER_172 = <any> 172,
        NUMBER_173 = <any> 173,
        NUMBER_174 = <any> 174,
        NUMBER_175 = <any> 175,
        NUMBER_176 = <any> 176,
        NUMBER_177 = <any> 177,
        NUMBER_178 = <any> 178,
        NUMBER_179 = <any> 179,
        NUMBER_180 = <any> 180,
        NUMBER_181 = <any> 181,
        NUMBER_182 = <any> 182,
        NUMBER_183 = <any> 183,
        NUMBER_184 = <any> 184,
        NUMBER_185 = <any> 185,
        NUMBER_186 = <any> 186,
        NUMBER_187 = <any> 187,
        NUMBER_188 = <any> 188,
        NUMBER_189 = <any> 189,
        NUMBER_190 = <any> 190,
        NUMBER_191 = <any> 191,
        NUMBER_192 = <any> 192,
        NUMBER_193 = <any> 193,
        NUMBER_194 = <any> 194,
        NUMBER_195 = <any> 195,
        NUMBER_196 = <any> 196,
        NUMBER_197 = <any> 197,
        NUMBER_198 = <any> 198,
        NUMBER_199 = <any> 199,
        NUMBER_200 = <any> 200,
        NUMBER_201 = <any> 201,
        NUMBER_202 = <any> 202,
        NUMBER_203 = <any> 203,
        NUMBER_204 = <any> 204,
        NUMBER_205 = <any> 205,
        NUMBER_206 = <any> 206,
        NUMBER_207 = <any> 207,
        NUMBER_208 = <any> 208,
        NUMBER_209 = <any> 209,
        NUMBER_210 = <any> 210,
        NUMBER_211 = <any> 211,
        NUMBER_212 = <any> 212,
        NUMBER_213 = <any> 213,
        NUMBER_214 = <any> 214,
        NUMBER_215 = <any> 215,
        NUMBER_216 = <any> 216,
        NUMBER_217 = <any> 217,
        NUMBER_218 = <any> 218,
        NUMBER_219 = <any> 219,
        NUMBER_220 = <any> 220,
        NUMBER_221 = <any> 221,
        NUMBER_222 = <any> 222,
        NUMBER_223 = <any> 223,
        NUMBER_224 = <any> 224,
        NUMBER_225 = <any> 225,
        NUMBER_226 = <any> 226,
        NUMBER_227 = <any> 227,
        NUMBER_228 = <any> 228,
        NUMBER_229 = <any> 229,
        NUMBER_230 = <any> 230,
        NUMBER_231 = <any> 231,
        NUMBER_232 = <any> 232,
        NUMBER_233 = <any> 233,
        NUMBER_234 = <any> 234,
        NUMBER_235 = <any> 235,
        NUMBER_236 = <any> 236,
        NUMBER_237 = <any> 237,
        NUMBER_238 = <any> 238
    }
}
