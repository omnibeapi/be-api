/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


export interface OTAURL {
    Address?: string;
    URLType?: OTAURL.URLTypeEnum;
}
export namespace OTAURL {
    export type URLTypeEnum = 0 | 1 | 2 | 3;
    export const URLTypeEnum = {
        NUMBER_0: 0 as URLTypeEnum,
        NUMBER_1: 1 as URLTypeEnum,
        NUMBER_2: 2 as URLTypeEnum,
        NUMBER_3: 3 as URLTypeEnum
    }
}
