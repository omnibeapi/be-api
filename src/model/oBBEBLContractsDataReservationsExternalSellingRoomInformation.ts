/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { ReservationsPriceDay } from './oBBEBLContractsDataReservationsPriceDay';
import { ReservationsReservationRoomTaxPolicy } from './oBBEBLContractsDataReservationsReservationRoomTaxPolicy';


export interface ReservationsExternalSellingRoomInformation {
    ReservationRoomsTotalAmount?: number;
    ReservationRoomsPriceSum?: number;
    ReservationRoomsExtrasSum?: number;
    TotalTax?: number;
    PricesPerDay?: Array<ReservationsPriceDay>;
    TaxPolicies?: Array<ReservationsReservationRoomTaxPolicy>;
}
