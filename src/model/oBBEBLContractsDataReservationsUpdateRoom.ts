/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { ReservationsReservationGuest } from './oBBEBLContractsDataReservationsReservationGuest';


export interface ReservationsUpdateRoom {
    RoomType_UID?: number;
    DateFrom?: Date;
    DateTo?: Date;
    AdultCount?: number;
    ChildCount?: number;
    ChildAges?: Array<number>;
    Number?: string;
    Guest?: ReservationsReservationGuest;
    Rate_UID?: number;
}
