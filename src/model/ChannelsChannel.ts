/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface ChannelsChannel {
    UID?: number;

    Name?: string;

    IsBookingEngine?: boolean;

    IsExtended?: boolean;

    Description?: string;

    Enabled?: boolean;

    Type?: number;

    IATA_Number?: string;

    IATA_Name?: string;

    Revision?: string;

    OperatorType?: number;

    OperatorCode?: string;

    ChannelCode?: string;

    RealTimeType?: number;

}
