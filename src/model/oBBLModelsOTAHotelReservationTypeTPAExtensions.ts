/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { OTAOtherLoyaltyCard } from './oBBLModelsOTAOtherLoyaltyCard';
import { OTAPartialPayment } from './oBBLModelsOTAPartialPayment';
import { OTAPayPalPayment } from './oBBLModelsOTAPayPalPayment';


export interface OTAHotelReservationTypeTPAExtensions {
    PartialPayment?: OTAPartialPayment;
    LoyaltyCard?: string;
    LoyaltyLevelName?: string;
    OtherLoyaltyCard?: OTAOtherLoyaltyCard;
    PayPalPayment?: OTAPayPalPayment;
    IsMobile?: boolean;
    ReferralSourceURL?: string;
}
