/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
import { OTAErrorsType } from './oBBLModelsOTAErrorsType';
import { OTAHotelRatePlansType } from './oBBLModelsOTAHotelRatePlansType';
import { OTASuccess } from './oBBLModelsOTASuccess';
import { OTATPAExtensions } from './oBBLModelsOTATPAExtensions';
import { OTAWarningsType } from './oBBLModelsOTAWarningsType';


export interface OTAOTAHotelRatePlanRS {
    EchoToken?: string;
    TimeStamp?: Date;
    Target?: OTAOTAHotelRatePlanRS.TargetEnum;
    Version?: number;
    PrimaryLangID?: OTAOTAHotelRatePlanRS.PrimaryLangIDEnum;
    Success?: OTASuccess;
    WarningsType?: OTAWarningsType;
    ErrorsType?: OTAErrorsType;
    RatePlans?: OTAHotelRatePlansType;
    TPA_Extensions?: OTATPAExtensions;
}
export namespace OTAOTAHotelRatePlanRS {
    export type TargetEnum = 0 | 1;
    export const TargetEnum = {
        NUMBER_0: 0 as TargetEnum,
        NUMBER_1: 1 as TargetEnum
    }
    export type PrimaryLangIDEnum = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8;
    export const PrimaryLangIDEnum = {
        NUMBER_1: 1 as PrimaryLangIDEnum,
        NUMBER_2: 2 as PrimaryLangIDEnum,
        NUMBER_3: 3 as PrimaryLangIDEnum,
        NUMBER_4: 4 as PrimaryLangIDEnum,
        NUMBER_5: 5 as PrimaryLangIDEnum,
        NUMBER_6: 6 as PrimaryLangIDEnum,
        NUMBER_7: 7 as PrimaryLangIDEnum,
        NUMBER_8: 8 as PrimaryLangIDEnum
    }
}
