/**
 * BE.API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface OTADiscount {
    NightsRequired?: number;

    NightsDiscounted?: number;

    DiscountPattern?: string;

    Percent?: number;

    ChargeUnitCode?: OTADiscount.ChargeUnitCodeEnum;

    AmountBeforeTax?: number;

    DiscountReason?: models.OTADiscountReason;

    DiscountCode?: string;

}
export namespace OTADiscount {
    export enum ChargeUnitCodeEnum {
        NUMBER_7 = <any> 7,
        NUMBER_12 = <any> 12,
        NUMBER_18 = <any> 18,
        NUMBER_19 = <any> 19,
        NUMBER_20 = <any> 20,
        NUMBER_21 = <any> 21,
        NUMBER_25 = <any> 25,
        NUMBER_99 = <any> 99
    }
}
