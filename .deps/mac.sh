#!/bin/bash

#######################
## Dependencies Installer for BE-API
## System: MacOS
#######################
declare -a deps=("brew" "git" "jq" "swagger-codegen")
declare -a bdeps=("upgrade git" "upgrade jq")

## Formatting
e="\033[0m"          # End Color
g="\033[92m\033[1m"  # Green Color 
r="\033[91m\033[1m"  # Red Color
o="\033[38;5;208m\033[1m"  # Orange Color

echo "${o}Running on a Mac :)$e"

## ECHO INFO
echo "\nRequired Dependencies:"
for i in "${deps[@]}"
do
  echo "${i}"
done

echo ""

if hash brew 2>/dev/null
then 
  echo "${g}Brew Installed $e"
  nbrew=0
else
  echo "${r}Brew Not Installed $e"
  nbrew=1
fi

if hash git 2>/dev/null
then 
  echo "${g}Git Installed $e"
  ngit=0
else
  echo "${r}Git Not Installed $e"
  ngit=1
fi


if hash jq 2>/dev/null
then 
  echo "${g}jq Installed $e"
  njq=0
else
  echo "${r}jq Not Installed $e"
  njq=1
fi

if hash swagger-codegen 2>/dev/null
then 
  echo "${g}swagger-codegen Installed $e"
  nscg=0
else
  echo "${r}swagger-codegen Not Installed $e"
  nscg=1
fi

if [ $nbrew -eq 1 ] || [ $ngit -eq 1 ] || [ $njq -eq 1 ]
then
  echo "Do you wish to run the ${g}Installation$e script?"
  select yn in "Yes" "No"; do
    case $yn in
      Yes ) 
        echo "Starting Installer..."
        break;;
      No ) 
        echo "Leaving Script..." 
        exit;;
    esac
  done
else
  echo "All Dependencies Installed"
  if [ "$1" != "" ]; then
    exit
  fi

  echo "Do you wish to run the ${o}Update$e script?"
  select yn in "Yes" "No"; do
    case $yn in
      Yes ) 
        echo "Upgrading Brew..."
        brew update        
        for i in "${bdeps[@]}"; do 
          brew "${i}" 2>/dev/null
        done
        echo "Done!\nLeaving Script..."         
        exit;;
      No ) 
        echo "Leaving Script..." 
        exit;;
    esac
  done
fi

## INSTALLING DEPENDENCIES
if [ $nbrew -eq 1 ] 
then
  echo "${g}#### Installing Brew ####$e"
  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

if [ $ngit -eq 1 ] 
then
  echo "${g}#### Installing Git ####$e"
  brew install git
fi

if [ $njq -eq 1 ] 
then
  echo "${g}#### Installing jq ####$e"
  brew install jq
fi

if [ $nscg -eq 1 ] 
then
  echo "${g}#### Installing swagger-codegen ####$e"
  brew install swagger-codegen
fi

sh ./deps.sh

