core=$(cat be-api.json | jq '.version.core')
major=$(cat be-api.json | jq '.version.major')
minor=$(cat be-api.json | jq '.version.minor')
version=${core}.${major}.${minor}

let "new_minor = $minor + 1"
new_version=${core}.${major}.${new_minor}

echo "Current Version : ${version}"

echo "Checking Dependencies..."
sh deps.sh 1

echo "Getting Last Update..."
git pull -s recursive -X theirs

./generate.sh src

echo 'Reverting variables.ts (pre-5)'
git checkout src/variables.ts

git add *

echo 'Changes : '
git status | grep modified

echo 'New Files : '
git status | grep 'new file'

# Updating be-api.json and writing to CHANGELOG
new_api=$(cat be-api.json | jq ".version.minor=${new_minor}")
new_cmd=$(echo '.version="'${new_version}'"')
new_package=$(cat package.json | jq ${new_cmd})
echo ${new_api} > be-api.json
echo ${new_package} > package.json

echo "\n#####NEW VERSION ${new_version}#####" >> CHANGELOG
echo "NEW_FILES:" >> CHANGELOG
git status | grep 'new file'  >> CHANGELOG
echo "CHANGED_FILES" >> CHANGELOG
git status | grep modified  >> CHANGELOG

echo 'Commiting...'
git commit -a -m "update V${new_version}"
git tag v${new_version}

echo 'Pushing...'
 git push
 git push --tags
