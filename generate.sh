if [ "$1" != "" ]; then

    # Generate code 
    swagger-codegen generate -i http://beapi-qa.omnibees.test/swagger/docs/v1 -l typescript-angular -DuseHttpClient -c config.json -o $1

    cd $1
    echo 'Working @ ' $PWD

    # Do Replaces
    echo 'Replacing OBBEBLContractsData'
    find . -type f ! -name '*.ts-e' -name '*.ts'  -exec sed -i -e 's/OBBEBLContractsData//g' {} +
    find . -type f ! -name '*.ts-e' -name '*.ts'  -execdir bash -c 'mv -i -f "$1" "${1//OBBEBLContractsData/}"' bash {} \;
    echo 'Replaced OBBEBLContractsData'

    echo 'Replacing OBBEBLContractsRequests'
    find . -type f ! -name '*.ts-e' -name '*.ts'  -exec sed -i -e 's/OBBEBLContractsRequests//g' {} +
    find . -type f ! -name '*.ts-e' -name '*.ts'  -execdir bash -c 'mv -i -f "$1" "${1//OBBEBLContractsRequests/}"' bash {} \;
    echo 'Replaced OBBEBLContractsRequests'

    echo 'Replacing OBBEBLContractsResponses'
    find . -type f ! -name '*.ts-e' -name '*.ts'  -exec sed -i -e 's/OBBEBLContractsResponses//g' {} +
    find . -type f ! -name '*.ts-e' -name '*.ts'  -execdir bash -c 'mv -i -f "$1" "${1//OBBEBLContractsResponses/}"' bash {} \;
    echo 'Replaced OBBEBLContractsResponses'

    echo 'Replacing OBBLModels'
    find . -type f ! -name '*.ts-e' -name '*.ts'  -exec sed -i -e 's/OBBLModels//g' {} +
    find . -type f ! -name '*.ts-e' -name '*.ts'  -execdir bash -c 'mv -i -f "$1" "${1//OBBLModels/}"' bash {} \;
    echo 'Replaced OBBLModels'


    find . -type f -name '*.ts-e' -exec rm -f {} +

    cd ..
    echo 'Back to ' $PWD

else 
    echo 'Missing PATH'
    echo 'e.g. ./generate.sh ./src'
fi
